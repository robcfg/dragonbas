# DragonBas #

This is a small utility I wrote for translating tokenized Dragon BASIC files to their text equivalents.

### How do I compile it? ###

Easy. On MacOS or Linux, just type **g++ -o dragonbas DragonBas.cpp**

On Windows, with Visual Studio installed, type **cl /EHsc DragonBas.cpp**

### How do I use it? ###

Even easier! 

On MacOS or Linux, type **./DragonBas <input.bin> <output.bas>**
On Windows, type **DragonBas.exe <input.bin> <output.bas>**

Where <input.bin> is the name of the tokenized Dragon BASIC file and <output.bas> the name of the file that will contain the translated BASIC program.

### Anything else? ###

I hope you like it and find it useful!
