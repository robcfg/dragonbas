// DragonBas - Conversion from Dragon tokenized binary BASIC files to BASIC text files
#include <cstdio>
#include <iostream>
#include <fstream>
#include <string>

using namespace std;

#ifndef _WIN32
#define _stricmp strcasecmp
#endif

#define TOKEN_ENDOFLINE         0x00
#define TOKEN_START             0x80
#define TOKEN_DUMMY             0xFE
#define TOKEN_FUNCTION          0xFF
#define TOKEN_MAX               0xCD
#define TOKEN_MAX_DDOS          0xE7
#define TOKEN_FUNCTION_MAX      0xA1
#define TOKEN_FUNCTION_MAX_DDOS 0xA8

// Token index is byte - 0x80
string reservedWords[] = {
	"FOR",
	"GO",
	"REM",
	"'",
	"ELSE",
	"IF",
	"DATA",
	"PRINT",
	"ON",
	"INPUT",
	"END",
	"NEXT",
	"DIM",
	"READ",
	"LET",
	"RUN",
	"RESTORE",
	"RETURN",
	"STOP",
	"POKE",
	"CONT",
	"LIST",
	"CLEAR",
	"NEW",
	"DEF",
	"CLOAD",
	"CSAVE",
	"OPEN",
	"CLOSE",
	"LLIST",
	"SET",
	"RESET",
	"CLS",
	"MOTOR",
	"SOUND",
	"AUDIO",
	"EXEC",
	"SKIPF",
	"DEL",
	"EDIT",
	"TRON",
	"TROFF",
	"LINE",
	"PCLS",
	"PSET",
	"PRESET",
	"SCREEN",
	"PCLEAR",
	"COLOR",
	"CIRCLE",
	"PAINT",
	"GET",
	"PUT",
	"DRAW",
	"PCOPY",
	"PMODE",
	"PLAY",
	"DLOAD",
	"RENUM",
	"TAB(",
	"TO",
	"SUB",
	"FN",
	"THEN",
	"NOT",
	"STEP",
	"OFF",
	"+",
	"-",
	"*",
	"/",
	"^",
	"AND",
	"OR",
	">",
	"=",
	"<",
	"USING",
    // Dragon DOS reserved Words
    "AUTO",
    "BACKUP",
    "BEEP",
    "BOOT",
    "CHAIN",
    "COPY",
    "CREATE",	
    "DIR",
    "DRIVE",
    "DSKINIT",
    "FREAD",
    "FWRITE",
    "ERROR",
    "KILL",
    "LOAD",
    "MERGE",
    "PROTECT",
    "WAIT",
    "RENAME",
    "SAVE",
    "SREAD",
    "SWRITE",
    "VERIFY",
    "FROM",
    "FLREAD",
    "SWAP"
};

// Function tokens are also indexed as byte - 0x80, but
// preceded by an 0xFF byte
string functionTokens[] = {
    "SGN",
    "INT",
    "ABS",
    "POS",
    "RND",
    "SQR",
    "LOG",
    "EXP",
    "SIN",
    "COS",
    "TAN",
    "ATN",
    "PEEK",
    "LEN",
    "STR$",
    "VAL",
    "ASC",
    "CHR$",
    "EOF",
    "JOYSTK",
    "FIX",
    "HEX$",
    "LEFT$",
    "RIGHT$",
    "MID$",
    "POINT",
    "INKEY$",
    "MEM",
    "VARPTR",
    "INSTR",
    "TIMER",
    "PPOINT",
    "STRING$",
    "USR",
    // Dragon DOS functions
    "LOF",
    "FREE",
    "ERL",
    "ERR",
    "HIMEM",
    "LOC",
    "FRE$"
};

bool ParseLine( ifstream& _in, 
                ofstream& _out, 
                unsigned short int& _lineAddress,
                bool _bIgnoreLineHeader = false, 
                bool _bShowLineAddresses = false,
                int _maxToken = TOKEN_MAX, 
                int _maxFunctionToken = TOKEN_FUNCTION_MAX )
{
    unsigned char token = 0;

    // Parse header (Pointer + line number)
    if( !_bIgnoreLineHeader )
    {
        token = (unsigned char)_in.get();

        if( _bShowLineAddresses )
        {
            _out << "[" << hex << uppercase << _lineAddress << "] " << nouppercase << dec;
        }

        // Skip 1st word or detect end of file
        unsigned char token2 = (unsigned char)_in.get(); 
        if( 0 == token && 0 == token2 )
        {
            return false;
        }
        
        _lineAddress = (token << 8) | token2;

        // Read line number
        int lineNumber = (_in.get() * 256) + _in.get();
        _out << lineNumber << " ";
    }

    // Parse tokens
    token = (unsigned char)_in.get();

    if( _in.eof() )
        return false;

    do
    {
        if( token == TOKEN_FUNCTION )
        {
            token = (unsigned char)_in.get();
            if( token >= TOKEN_START && token <= _maxFunctionToken)
                _out << functionTokens[token - TOKEN_START];
        }
        else if( token >= TOKEN_START )
        {
            if( token >= TOKEN_START && token <= _maxToken)
                _out << reservedWords[token - TOKEN_START];
        }
        else if( token >= 0x20 && token < TOKEN_START )
        {
            _out << (char)token;
        }

        token = (unsigned char)_in.get();
    }
    while( token != TOKEN_ENDOFLINE && !_in.eof() );

    _out << endl;

    return true;
}

int main(int argc, char** argv)
{
    // Check arguments
    if( argc < 3 )
    {
        cout << "Too few arguments!" << endl << endl;
        cout << "Usage:" << endl;
        cout << "    dragonbas input_file output_file [options]" << endl << endl;
        cout << "Options:" << endl;
        cout << "    -ignorelineheader ignores the line pointer and line number header when parsing." << endl;
        cout << "    -dragondos uses extra tokens provided by the DragonDOS disk interface." << endl;
        cout << "    -lineaddresses Show the memory address of the BASIC code lines." << endl << endl;
        cout << "E.g.:" << endl;
        cout << "    dragonbas in.bin out.bas" << endl << endl;

        return 0;
    }

    // Open input and output files
    ifstream in(argv[1],ios_base::binary);
    if( !in.is_open() )
    {
        cout << "Couldn't open " << argv[1] << " for reading." << endl;
        return 1;
    }

    ofstream out(argv[2]);
    if( !out.is_open() )
    {
        cout << "Couldn't open " << argv[2] << " for writing." << endl;
        in.close();
        return 1;
    }

    // Parse arguments
    bool ignorelineheader   = false;
    bool showLineAddresses  = false;
    int maxToken            = TOKEN_MAX;
    int maxTokenFunction    = TOKEN_FUNCTION_MAX;

    for( int iArg = 3; iArg < argc; ++iArg )
    {
        if( 0 == _stricmp(argv[iArg],"-ignorelineheader") ) ignorelineheader = true;
        else if( 0 == _stricmp(argv[iArg],"-dragondos") )
        {
            maxToken         = TOKEN_MAX_DDOS;
            maxTokenFunction = TOKEN_FUNCTION_MAX_DDOS; 
        }
        else if( 0 == _stricmp(argv[iArg],"-lineaddresses") )
        {
            showLineAddresses = true;
        }
    }

    // Parse tokens
    unsigned char token = 0;
    unsigned short int programStart = 0x1E01;

    while(ParseLine( in, out, programStart, ignorelineheader, showLineAddresses, maxToken, maxTokenFunction ));

    // Close files
    out.close();
    in.close();

    // Good bye!
    return 0;
}